resource "azurerm_network_security_group" "nsg" {
  name                = "nsg-${var.name}"
  location            = var.rg_location
  resource_group_name = var.rg_name
}

locals {
  source_prefix_dest_port = var.security_rules == null ? toset({}) : { for name, rule in var.security_rules : name => rule if length(rule.source_address_prefixes) == 1 && length(rule.destination_port_ranges) == 1 }
  msource_prefix_dest_port = var.security_rules == null ? toset({}) : { for name, rule in var.security_rules : name => rule if length(rule.source_address_prefixes) > 1  && length(rule.destination_port_ranges) == 1 }
  source_prefix_mdest_port =  var.security_rules == null ? toset({}) : { for name, rule in var.security_rules : name => rule if length(rule.source_address_prefixes) == 1 && length(rule.destination_port_ranges) > 1 }
  msource_prefix_mdest_port = var.security_rules == null ? toset({}) : { for name, rule in var.security_rules : name => rule if length(rule.source_address_prefixes) > 1  && length(rule.destination_port_ranges) > 1 }
}

resource "azurerm_network_security_rule" "nsg-rule" {
  for_each                    = var.security_rules != null ? local.source_prefix_dest_port : toset({})
  name                        = each.key
  priority                    = each.value.priority
  direction                   = each.value.direction
  access                      = each.value.access
  protocol                    = each.value.protocol
  source_address_prefix       = element(each.value.source_address_prefixes, 0)
  destination_address_prefix  = element((each.value.destination_address_prefixes != null ? each.value.destination_address_prefixes : var.default_destination_address_prefixes), 0)
  source_port_range           = each.value.source_port_range
  destination_port_range      = element(each.value.destination_port_ranges, 0)
  resource_group_name         = var.rg_name
  network_security_group_name = azurerm_network_security_group.nsg.name
}

resource "azurerm_network_security_rule" "nsg-rule-multi-source-prefix" {
  for_each                    = var.security_rules != null ? local.msource_prefix_dest_port : toset({})
  name                        = each.key
  priority                    = each.value.priority
  direction                   = each.value.direction
  access                      = each.value.access
  protocol                    = each.value.protocol
  source_address_prefixes     = each.value.source_address_prefixes
  destination_address_prefix  = element((each.value.destination_address_prefixes != null ? each.value.destination_address_prefixes : var.default_destination_address_prefixes), 0)
  source_port_range           = each.value.source_port_range
  destination_port_range      = element(each.value.destination_port_ranges, 0)
  resource_group_name         = var.rg_name
  network_security_group_name = azurerm_network_security_group.nsg.name
}

resource "azurerm_network_security_rule" "nsg-rule-source-prefix-multi-dest-range" {
  for_each                    = var.security_rules != null ? local.source_prefix_mdest_port : toset({})
  name                        = each.key
  priority                    = each.value.priority
  direction                   = each.value.direction
  access                      = each.value.access
  protocol                    = each.value.protocol
  source_address_prefix       = element(each.value.source_address_prefixes, 0)
  destination_address_prefix  = element((each.value.destination_address_prefixes != null ? each.value.destination_address_prefixes : var.default_destination_address_prefixes), 0)
  source_port_range           = each.value.source_port_range
  destination_port_ranges     = each.value.destination_port_ranges
  resource_group_name         = var.rg_name
  network_security_group_name = azurerm_network_security_group.nsg.name
}

resource "azurerm_network_security_rule" "nsg-rule-multi-source-prefix-multi-dest-range" {
  for_each                    = var.security_rules != null ? local.msource_prefix_mdest_port : toset({})
  name                        = each.key
  priority                    = each.value.priority
  direction                   = each.value.direction
  access                      = each.value.access
  protocol                    = each.value.protocol
  source_address_prefixes     = each.value.source_address_prefixes
  destination_address_prefix  = element((each.value.destination_address_prefixes != null ? each.value.destination_address_prefixes : var.default_destination_address_prefixes), 0)
  source_port_range           = each.value.source_port_range
  destination_port_ranges     = each.value.destination_port_ranges
  resource_group_name         = var.rg_name
  network_security_group_name = azurerm_network_security_group.nsg.name
}
