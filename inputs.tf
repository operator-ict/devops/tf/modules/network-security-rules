variable "name" {
  type = string
}

variable "rg_name" {
  type = string
}

variable "rg_location" {
  type = string
}

variable "default_destination_address_prefixes" {
  type = list(string)
  default = []
}

variable "security_rules" {
  type = map(object({
    priority                      = number
    destination_address_prefixes  = optional(list(string))
    destination_port_ranges       = list(string)
    access                        = optional(string, "Allow")
    source_address_prefixes       = optional(list(string), ["Internet"])
    source_port_range             = optional(string, "*")
    direction                     = optional(string, "Inbound")
    protocol                      = optional(string, "Tcp")
  })
  )
  default = {}
}
